import 'package:flutter/material.dart';

class NavButton extends StatelessWidget {
  final Function _changePage;
  final String _title;

  NavButton(this._changePage, this._title);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.green[500],
        borderRadius: BorderRadius.circular(10.0),
        boxShadow: [
          BoxShadow(
            color: Colors.green,
            blurRadius: 10.0,
            spreadRadius: 0.0,
            offset: Offset(
              4.0, // horizontal
              3.0, // vertical
            ),
          ),
        ],
      ),
      padding: const EdgeInsets.all(10.0),
      height: 50.0,
      width: 200.0,
      child: InkWell(
        onTap: _changePage,
        child: Center(
          child: Text(
            _title,
            style: TextStyle(fontSize: 20.0, color: Colors.white),
          ),
        ),
      ),
    );
  }
}
