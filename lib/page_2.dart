import 'package:flutter/material.dart';
import './widgets/nav_button.dart';

class SecondPage extends StatelessWidget {

  showAlertDialog(BuildContext context) {
    BuildContext dialogContext = context;
    Widget okButton = FlatButton(
      child: Text("OK, I'll try"),
      onPressed: () {
        Navigator.pop(dialogContext);
      },
    );

    AlertDialog alert = AlertDialog(
      title: Text("Hello"),
      content: Text("Be nice and have a good day"),
      actions: [
        okButton,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Page 2')),
      body: Center(
        child: NavButton(() => showAlertDialog(context), 'Display dialog'),
      ),
    );
  }
}